# Compilers
CXX = g++
CC = gcc

# Flags to the C/C++ compiler
CFLAGS = -Wall -Wextra -fopenmp -g -fmax-errors=3

# Header include directories (include the -I)
IFLAGS =

# Linker flags
LFLAGS = -larmadillo

# Source search paths (locations containing .cpp/.hpp)
VPATH = src

# Messages about CFLAGS and LFLAGS (see patterns.mk)
MESSAGES = opt warn parallel lib debug cflags lflags

# Dependency directories
DEPS = deps
OBJDIR = obj

# Executables list
EXE = output test cmix conly

# List output executables with object file dependencies
output: main.o foo.o bar.o
test: test.o foo.o bar.o more.o
cmix: cmix.o foo.o cfile.o
conly: conly.o cfile.o

# Include makefile patterns and dependency information
include patterns.mk
-include $(wildcard $(DEPS)/*.d)
