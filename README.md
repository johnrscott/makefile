# Automatic Dependency Makefile for C++ Projects

This repository contains a makefile for compiling C++ projects, which handles dependencies automatically. It allows for multiple object files, and multiple output executable targets, each of which may use different subsets of object files.

The system uses two files: `Makefile`, `patterns.mk`. The `Makefile` contains the list of object files, specifies output executable names and contains compiler and linker flags, include paths, etc. The `patterns.mk` contains rules for compiling and linking the program, and does not need to be modified.

## Setup
1. Modify the variables in `Makefile`: 
   1. The `CFLAGS` are used for all parts of the build process, including the linking stage.
   2. The `IFLAGS` need to be specified as a space separated list of paths, which include the `-I` flag (it is passed directly to `$(CC)`). They are used for compiling all the object files (where header files are used), but not for the linking step.
   3. The `LFLAGS` can be used to specify linking commands, e.g. `-l` to link a library or `-L` to specify a library path. They are used once, only at the linking step.
   4. The `VPATH` is used to specify the locations of source files, as a space separated list of paths. 
   5. The `DEPS` and `OBJDIR` are used to specify names for folders which store object files and dependency information. Both can be hidden if desired (e.g. by using filenames `.deps` and `.obj`.)
2. Add one line for each output executable in the form `output: main.o foo.o bar.o`. One of the object files needs to contain `main`. Do not specify the compilation line, only the prerequisites. Prerequisites that are not object files are also allowed (e.g. a linker script that is included in `LFLAGS`). Add all the output names to the `EXE` variable so that they can be deleted using `make clean`.
3. The system produces `.d` files for dependencies and `.o` files for objects. These can be ignored using `.gitignore`. 

## Use 
Run `make main`. _Make_ will build all the object files listed for `main`, as well as building `main.o`, and link them to produce `main`. It will also automatically generate the object and dependency folders and work out dependency information for each object file. The dependency information is then used to determine when object files need recompiling. Makefile targets should tab complete as normal. Running `make` will compile the first target listed in `Makefile`.

Run `make clean` to delete all object files and dependency information. Currently, `clean` does not remove the executables.

The object and dependency folders can be deleted at any time. They will be recreated then next time _make_ runs.

Changes to the `Makefile` will cause all the object files to be rebuilt. This allows flags like `CFLAGS` to be changed without worrying about making clean.

## Mixing C and C++

The `patterns.mk` makefile supports files written in C or C++. In both cases, the object file is specified as `file.o`. The makefile will use `$(CC)` if `file.c` is present, and `$(CXX)` if `file.cpp` is present. Automatic dependencies still work for all combinations of C and C++ header files.

A project that is entirely written in C will still be linked using `$(CXX)`. This doesn't cause any problems because ultimately the linking step invokes `ld` to do the work anyway.

## How it works

The makefiles use pattern matching rules to compile object files and link the final executable. The object file compilation rule incorporates automatic dependency generation.

Clear information about all the features of _make_ are given in the [manual](https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents). The makefiles use a number of special make features. Links to the relevant parts of the documentation are provided below.

The makefile design has the following features
 - Use is made of the [include](https://www.gnu.org/software/make/manual/html_node/Include.html#Include) directive. The dependency information is gathered from many different small makefiles (with extension `*.d`). Including works like the C preprocessor; included file contents is copied verbatim to the location of the include.
 - Object file targets (appearing in prerequisite lists) are given as filenames without including their full path. Rules for making object files write to the object file directory (using `-o $(OBJDIR)/$@`, see [automatic variables](https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables)). This means that the file that is generated (e.g. `obj/file.o`) has a different name to the target (`file.o`). Hence, _make_ needs to search the object file directory when determining whether the target `file.o` is up to date. This is accomplished using the [vpath](https://www.gnu.org/software/make/manual/html_node/Selective-Search.html#Selective-Search) directive, which can be specifically targetted to match object files. More general searches for source files use the [`VPATH`](https://www.gnu.org/software/make/manual/html_node/General-Search.html#General-Search) in `Makefile`.
 - Dependencies are generated using gcc [automatic dependency flags](https://gcc.gnu.org/onlinedocs/gcc/Preprocessor-Options.html) (most of which begin `-M`) and stored in `*.d` files ready to be included in `Makefile`.
 - Compilation and linking are performed using [pattern rules](https://www.gnu.org/software/make/manual/html_node/Pattern-Intro.html#Pattern-Intro), which are used for building targets that match a particular pattern (for example, object file match `%.o`). An important feature of patterns is the ability to split up the prerequisites from the rule itself. This means that the user can specify specific targets and prerequisites in `Makefile`, whereas the actual rule that performs the compilation is stored in `patterns.mk`.
 
Further detailed information information about the patterns are given below.

### Compilation and dependencies

Object file compilation is performed using two pattern rules in `patterns.mk` (one for C++, the other for C)

```makefile
# For cpp files
%.o: %.cpp $(MAKEFILES) | $(OBJDIR) $(DEPS)
	$(CXX) $(CFLAGS) $(IFLAGS) $(DFLAGS) -c $< -o $(OBJDIR)/$@
# For c files
%.o: %.c $(MAKEFILES) | $(OBJDIR) $(DEPS)
	$(CC) $(CFLAGS) $(IFLAGS) $(DFLAGS) -c $< -o $(OBJDIR)/$@
```

It matches prerequisites like `obj.o`, and automatically includes `obj.cpp` or `obj.c` as a dependency, depending which is present. The makefiles (`Makefile`, `patterns.mk`) are also prerequisites. The vertical bar means that the prerequisites that follow (the object and dependency folders) are [order only](https://www.gnu.org/software/make/manual/html_node/Prerequisite-Types.html) and  will not cause object file recompilation. The result of the compilation is stored in the object file directory.

All the flags come from the `Makefile` file, except the dependency flags:

```makefile
DFLAGS = -MMD -MT $*.o -MF $(DEPS)/$*.d
```

The `-MF` flags specifies the output file in which to store the dependency information. The dependency files (`*.d`) have the following form:

```makefile
file.o: file.cpp dep1.hpp dep2.hpp dep3.hpp ...
```

The list includes all the header files which are included in any way in `file.cpp` They are included directly in `Makefile`, allowing _make_ to determine if object files need updating.

### Linking

The decision to use an object compilation rule which outputs a different file to its target (`-o $(OBJDIR)/$@` rather than `-o $@`) inevitably leads to problems. (See [this article](http://make.mad-scientist.net/papers/rules-of-makefiles/) for reference. The current makefile breaks rules 2 and 3. It also basically does the opposite of [this advice](http://make.mad-scientist.net/papers/how-not-to-use-vpath/).) The main issue is that _make_ starts substituting full paths for object file prerequisites found using `VPATH`, where previously a bare filename would have appeared. This leads to problems for the linking step, which needs to correctly interpret the prerequisites list (in the variable `$^`) and pass it to `$(CXX)`.

This reason for using bare object file names and is that 
1. object files can be stored in an object file directory, while retaining the ability to:
2. simply list object files using their name in the main makefile. This avoids this need for `addprefix` to turn an object list into a prerequisite list, which makes it easier to give different output executables different combinations of object file dependencies. 

These two benefits are sufficient justification for the added complexity of the `pattern.mk` file. (See [this article](http://make.mad-scientist.net/papers/multi-architecture-builds/) for an alternative method to implement the two objectives above.)

Linking is performed by another pattern rule in `patterns.mk`, which matches the output executable line (it is a [match anything rule](https://www.gnu.org/software/make/manual/html_node/Match_002dAnything-Rules.html#Match_002dAnything-Rules), to allow for flexibility in the final executable name):

```makefile
%:
	$(CXX) $(CFLAGS) $(call fixPath, $^) -o $@ $(LFLAGS)
```

The `LFLAGS` are used here to specify linking options. There are no prerequisites listed here; the prerequisites used are the same as those listed in `Makefile` for the output executable. Any prerequisites that are not object files are filtered out. Two cases arise for each remaining object file:
  - if the object file does not exist, then it is built and placed in the `$(OBJDIR)`. However, the prerequisites list (as in the `Makefile`) contains object files without paths. Therefore, this directory needs to be prefixed to the object file name before linking occurs (performed by the `addprefix` command), otherwise `$(CXX)` will not be able to find the object files.
  - if the object file already exists, then `VPATH` will substitute the full path (including the `$(OBJDIR)`) in the prerequisite list. That means that the `addprefix` command above will add another `$(OBJDIR)`, which will _make_ the wrong path. It is therefore necessary to remove the path in advance, using the `notdir` command. 

These actions are performed by the following [function](https://www.gnu.org/software/make/manual/html_node/Call-Function.html#Call-Function)

```makefile
define fixPath
$(addprefix $(OBJDIR)/, $(notdir $(filter %.o, $(1))))
endef
```

### More on automatic dependencies

The implementation of the automatic dependencies is taken from [this article](http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/), which includes an overview of different ways to handle dependencies. The crux of the method is that 
1. The `Makefile` includes dependency information from `*.d` files every time it runs
2. each time an object file is compiled, dependency information is re-generated and stored in the corresponding `*.d` file

The following points may help to explain in more detail why the system works:

1. Only object files have dependencies, because they are the things that need to be recompiled. Every object file depends on one and only one cpp file, and the tree of hpp files that it includes.

2. The GCC automatic dependency generation looks at all the user header files that are included in the cpp file corresponding to the current object file. These are what is listed in the dependency file.

3. The first time _make_ runs, there is no dependency information. However, there are also no object files, so they all need to be built anyway. In the process of building the object files, dependency information is gathered.

4. Any modification to any of the tree of included header files (including adding or removing header file includes) will cause object file recompilation, because these dependencies are recorded in the `*.d` file. Deleting or adding a header file in the cpp file (i.e. modifying the dependency structure) also causes recompilation, because the object file depends on the cpp file. This compilation will then produce up to date header information.

### TODO list

1. Add an option to automatically print the optimisation level in bold to avoid mistakes. 
2. Have the makefile recognise when the branch has changed in a git project, and trigger an automatic clean. Sometimes the makefile causes subtle bugs when the branch changes and object files are not properly cleaned (since object files are probably gitignored, there may be object files left over from the old branch which do not get recompiled).
