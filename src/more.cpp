#include "foo.hpp"
#include "bar.hpp"
#include <iostream>

void more() {
    foo();
    bar();
    std::cout << "Even more" << std::endl;
}
